
Upstream package of dnswalk distributes rfc1912.txt alongside
the program for convenient viewing. 
As one might read the licence of the RFCs to not be DFSG 
compliant I removed that RFC from the Debian package.
See Bug #199804 for details ( http://bugs.debian.org/199804 ).

You may find the RFC inside the Debian RFC packages which
reside in non-free currently ( use apt-search rfc ) 
or use some internet search engine to find an online copy.

Florian Hinzmann <fh@debian.org>, 17.7.2003

